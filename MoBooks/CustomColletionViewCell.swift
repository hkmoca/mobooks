//
//  CustomColletionCell.swift
//  MoBooks
//
//  Created by Héctor Moreno on 17/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit

class CustomColletionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        
    }
    
    func updateUI(cellInfo: BaseDataFile){
        
        if let sectionImageURL = URL(string: cellInfo.imageUrl){
            imageView.af_setImage(withURL: sectionImageURL)
        }
        
    }
}
