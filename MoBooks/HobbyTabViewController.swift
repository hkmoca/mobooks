//
//  FileInfoTabCollectionVC.swift
//  MoBooks
//
//  Created by Héctor Moreno on 17/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SectionOfInterestCell"
private let segueIdentifier = "showDetails"

class HobbyTabViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    @IBOutlet weak var pickerSection: UIPickerView!
    @IBOutlet weak var sectionOfInterestButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var Hobbies: [BaseDataFile] = []
    var currentPage = 0
    let sectionsOfInterest = [Hobbie.TypeOfHobby.Arts.rawValue, Hobbie.TypeOfHobby.Food.rawValue]
    var collectionViewProperties = SetCollectionViewProperties()
    let getDataFromDataBase = GetDataFromDataBase()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewProperties.setCollectionViewProperties(collectionView: collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        pickerSection.dataSource = self
        pickerSection.delegate = self
        
    }
    
    
    @IBAction func getSectionOfInterestData(_ sender: Any) {
        pickerSection.isHidden = false
        Hobbies = []
        self.collectionView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueIdentifier {
            let indexPaths = collectionView?.indexPathsForSelectedItems
            let descriptionViewController =  segue.destination as? MediaDescription
            let indexPath: IndexPath? = (indexPaths?[0])
            descriptionViewController?.mediaFile = Hobbies[(indexPath?.row)!]
        }
    }
    
    func getJSONtoTableView(offSet: Int, section: String ){
        getDataFromDataBase.getDataInfoFotCollectionView(section: section) { (hobbies) in
            self.Hobbies = hobbies
            self.collectionView?.reloadData()
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sectionsOfInterest.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sectionsOfInterest[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sectionOfInterestButton.setTitle(sectionsOfInterest[row], for: .normal)
        getJSONtoTableView(offSet: currentPage, section: sectionsOfInterest[row])
        pickerSection.isHidden = true
    }
    
}

// MARK: UICollectionViewDataSource
extension HobbyTabViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Hobbies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? CustomColletionViewCell
        
        let cellIndex = Hobbies[indexPath.item]
        cell?.updateUI(cellInfo: cellIndex)
        
        return cell!
    }
}
