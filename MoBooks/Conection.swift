//
//  ConectionAlamoFire.swift
//  MoBooks
//
//  Created by Héctor Moreno on 01/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import Alamofire

class Conection {

   static func getDataBase(tabIndex: Int, offSet: Int, compleation: @escaping (_: [String : Any]) -> Void, onFailure: @escaping (_: NSError) -> Void){
        
        if tabIndex == 0 {
            
            Alamofire.request(Router.getMoviesDataBase(_offSet: offSet))
                .responseJSON { response in
                    
                    HandleResponseJSON.handleResponse(response, compleation: compleation, onFailure: onFailure)
                    
            }
        }
        
        if tabIndex == 1 {
            
            Alamofire.request(Router.getBooksDataBase(_offSet: offSet))
                .responseJSON { response in
                    
                    HandleResponseJSON.handleResponse(response, compleation: compleation, onFailure: onFailure)
                    
            }
        }
    }
    
    
    
  static func getSectionOfInterest(section: String, compleation: @escaping (_: [String : Any]) -> Void, onFailure: @escaping (_: NSError) -> Void){
        
        Alamofire.request(Router.getSectionOfInterest(_section: section))
            .responseJSON { response in
               
                HandleResponseJSON.handleResponse(response, compleation: compleation, onFailure: onFailure)
                
        }
    }
    
}
