//
//  SwitchManager.swift
//  MoBooks
//
//  Created by Héctor Moreno on 19/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation

import UIKit

class SwitchViewManager {
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static func switchToLogged() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "Main")
        appDelegate.window?.rootViewController = nav
        
    }
    
    static func switchToLogin(){
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "Login")
        appDelegate.window?.rootViewController = nav
        
    }
    
}
