//
//  SectionOfInterest.swift
//  MoBooks
//
//  Created by Héctor Moreno on 14/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import ObjectMapper

class Hobbie: BaseDataFile, Mappable {
    
    enum TypeOfHobby: String {
        
        case Arts, Food
        
    }
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    func mapping(map: Map) {
        title               <- map["title"]
        author              <- map["byline"]
        explanation         <- map["abstract"]
        imageUrl            <- map["media.0.media-metadata.2.url"]
        urlLink             <- map["url"]
        
    }
}
