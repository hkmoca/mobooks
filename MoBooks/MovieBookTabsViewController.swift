//
//  FirstViewController.swift
//  MoBooks
//
//  Created by Héctor Moreno on 24/01/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit
import RealmSwift

private let reuseIdentifier = "infoCell"
private let segueIdentifier = "showDetails"

class MovieBookTabsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    
    @IBOutlet weak var tableView: UITableView?
    let getDataFromDataBase = GetDataFromDataBase()
    var multimediaFiles: [BaseDataFile] = []
    var viewControllerIndex: Int?
    var currentPage = 0
    let realm = try! Realm()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllerIndex = tabBarController?.selectedIndex
        tableView?.dataSource = self
        tableView?.delegate = self
        checkConectionAndGetDataForRows()
        
    }
    
    func checkConectionAndGetDataForRows(){
        
        if ConnectionCheck.isConnectedToNetwork() {
            getJSONtoTableView(viewControllerIndex!, offSet: currentPage)
            
        } else {
            if viewControllerIndex == 0{
                multimediaFiles = Array(realm.objects(Movie.self))
            } else {
                multimediaFiles = Array(realm.objects(Book.self))
            }
            
            print("Hey!!!! check your internet conection")
        }
    }
    
    func getJSONtoTableView(_ tabIndex: Int, offSet: Int ){
        //Change name
        getDataFromDataBase.getDataInfoForTableView(tabIndex: tabIndex, offSet: currentPage, completion: { (multimedia) in
            self.multimediaFiles = multimedia
            self.tableView?.reloadData()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueIdentifier{
            let indexPath = tableView?.indexPathForSelectedRow
            let descriptionViewController =  segue.destination as? MediaDescription
            descriptionViewController?.mediaFile = multimediaFiles[indexPath!.row]
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return multimediaFiles.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? CustomTableViewCell
        let cellIndex = multimediaFiles[indexPath.row]
        cell?.updateFile(cellInfo: cellIndex)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row + 1 == multimediaFiles.count && ConnectionCheck.isConnectedToNetwork() {
            getMoreInfoOffset(viewControllerIndex!)
        }
    }
    
    func getMoreInfoOffset(_ tabIndex: Int){
        print("Geting 20 more files")
        currentPage += 20
        getJSONtoTableView(tabIndex, offSet: currentPage)
    }
}
