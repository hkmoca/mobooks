//
//  FacebookManager.swift
//  MoBooks
//
//  Created by Héctor Moreno on 28/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import FBSDKLoginKit

class FaceboolManager{
    
    
    static func getFacebookUserInformation(){
        
        let facebookUser = User()
        let parameters = ["fields" : "email, id, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start { (connection, result, error) in
            if error != nil {
                print(error!)
                return
            }
            let result = result as? NSDictionary
            if let email = result?["email"] as? String{
                facebookUser.email =  email
            }
            if let userFbID = result?["id"] as? String{
                facebookUser.userID = userFbID
            }
            if let firstName = result?["first_name"] as? String{
                facebookUser.firstName = firstName
            }
            if let lastName = result?["last_name"] as? String{
                facebookUser.lastName = lastName
            }
            
            print(facebookUser.email, facebookUser.userID, facebookUser.firstName, facebookUser.lastName)
            RealmManager.writeToDataBaseFacebookUser(facebookUser)
            SwitchViewManager.switchToLogged()
        }
        
    }
    
    
    static func setLoginButton(_ view: UIView, loginButton: FBSDKLoginButton){
        loginButton.center = view.center
        view.addSubview(loginButton)
    }
}
