//
//  DescriptionViewController.swift
//  MoBooks
//
//  Created by Héctor Moreno on 28/01/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit


class MediaDescription: UIViewController {
    
    var mediaFile: BaseDataFile!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var elementImage: UIImageView!
    @IBOutlet weak var linkButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillInformationRows()
        
    }
    
    @IBAction func openLinkInBrowser(_ sender: Any) {
        
        if let requestUrl = URL(string: mediaFile.urlLink ) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(requestUrl, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(requestUrl)
            }
        }
    }
    
    
    func fillInformationRows(){
        nameLabel.text = mediaFile.title
        author.text = mediaFile.author
        rank.text = mediaFile.rank
        movieDescription.text = mediaFile.explanation
        
        if mediaFile.urlLink == "" {
            linkButton.isHidden = true
        }
        
        if let imageURL = URL(string: mediaFile.imageUrl) {
            elementImage.af_setImage(withURL: imageURL)
        }
        
    }
}
