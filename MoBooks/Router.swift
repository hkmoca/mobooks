//
//  Router.swift
//  MoBooks
//
//  Created by Héctor Moreno on 01/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    
    
    static let baseURLString = "http://api.nytimes.com"
    
    case getMoviesDataBase(_offSet: Int)
    case getBooksDataBase(_offSet: Int)
    case getSectionOfInterest(_section: String)
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .getMoviesDataBase, .getBooksDataBase, .getSectionOfInterest:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getMoviesDataBase:
            return "/svc/movies/v2/reviews/dvd-picks.json"
        case .getBooksDataBase:
            return "/svc/books/v3/lists/best-sellers/history.json"
        case .getSectionOfInterest(let section):
            return "/svc/mostpopular/v2/mostviewed/\(section)/30.json"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .getMoviesDataBase(let offSet), .getBooksDataBase(let offSet):
            return [
                "api-key": "ef7c9be316a34ee58d0b344e1ddd9119",
                "offset": offSet
            ]
        default: return [
            "api-key": "ef7c9be316a34ee58d0b344e1ddd9119",
            "offset": 0
            ]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Router.baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getMoviesDataBase, .getBooksDataBase, .getSectionOfInterest:
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: parameters)
        }
    }
}
