//
//  Model.swift
//  MoBooks
//
//  Created by Héctor Moreno on 26/01/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import ObjectMapper

class Model {
    
  static func getDataArray(tabIndex: Int, offSet: Int, completion:  @escaping (_: [AnyObject]) -> Void, onFailure: @escaping (_: NSError) -> Void) {
        
        Conection.getDataBase(tabIndex: tabIndex, offSet: offSet, compleation: { (json) in
            let results = json["results"] as? [[String : Any]]
            
            if tabIndex == 0 {
                
                if let jsonResult = results {
                    let resultsArray: Array<Movie> = Mapper<Movie>().mapArray(JSONArray: jsonResult)
                    print("Got the json into mapping Array")
                    RealmManager.writeToRealmDataBase(resultsArray)
                    completion(resultsArray)
                } else {
                    print("Not the correct type")
                }
            }
            
            if tabIndex == 1 {
                
                if let jsonResult = results {
                    let resultsArray: Array<Book> = Mapper<Book>().mapArray(JSONArray: jsonResult)
                    RealmManager.writeToRealmDataBase(resultsArray)
                    completion(resultsArray)
                } else {
                    print("Not the correct type")
                }
            }
            
        }) { (NSError) in
            
        }
    }
    
   static func getSectionOfInterest(section: String, completion: @escaping (_: [Hobbie]) -> Void, onFailure: @escaping (_: NSError) -> Void) {
        
        Conection.getSectionOfInterest(section: section, compleation: { (json) in
            if let jsonResult = json["results"] as? [[String : Any]] {
                let results: Array<Hobbie> = Mapper<Hobbie>().mapArray(JSONArray: jsonResult)
                print("Got the json into mapping Array")
                RealmManager.writeToRealmDataBase(results)
                completion(results)
            } else {
                print("Not the correct type")
            }
        }) { (NSError) in
            
        }
        
    }
    
    
}
