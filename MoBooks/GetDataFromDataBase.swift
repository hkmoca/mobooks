//
//  GetDataFromDataBase.swift
//  MoBooks
//
//  Created by Héctor Moreno on 18/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//


import UIKit

class GetDataFromDataBase {
    
    var multimediaFiles: [BaseDataFile] = []
    
    func getDataInfoForTableView(tabIndex: Int, offSet: Int, completion: @escaping (_: [BaseDataFile]) -> Void) {
        
        Model.getDataArray(tabIndex: tabIndex, offSet: offSet, completion: {(multimedia) in
            let multimediaData = NSMutableArray(array: self.multimediaFiles)
            multimediaData.addObjects(from: multimedia)
            self.multimediaFiles = multimediaData as! [BaseDataFile]
            completion(self.multimediaFiles)
            print("Succes getting DataBase!")
        }, onFailure: { (NSError) in
            print("Something went wrong")
        })
    }
    
    func getDataInfoFotCollectionView(section: String, completion: @escaping (_: [BaseDataFile]) -> Void){
    
        multimediaFiles = []
        Model.getSectionOfInterest(section: section, completion: { (Hobbies) in
            let sectionOfInterestData = NSMutableArray(array: self.multimediaFiles)
            sectionOfInterestData.addObjects(from: Hobbies)
            self.multimediaFiles = sectionOfInterestData as! [BaseDataFile]
            completion(self.multimediaFiles)
            print("Succes getting art or food!")
        }, onFailure: { (NSError) in
            print("Something went wrong")
        })
    }
}
