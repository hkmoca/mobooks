//
//  User.swift
//  MoBooks
//
//  Created by Héctor Moreno on 26/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//


import UIKit
import RealmSwift

class User: Object {
    
    dynamic var email: String = ""
    dynamic var userID: String = ""
    dynamic var firstName: String = ""
    dynamic var lastName: String = ""
    
    override static func primaryKey() -> String? {
        return "email"
    }
    
}
