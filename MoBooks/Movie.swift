//
//  Movie.swift
//  MoBooks
//
//  Created by Héctor Moreno on 25/01/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Movie: BaseDataFile, Mappable {
    
    required convenience init?(map: Map) {
        self.init()
        
    }

    func mapping(map: Map) {
        title               <- map["display_title"]
        rank                <- map["mpaa_rating"]
        author              <- map["byline"]
        explanation         <- map["summary_short"]
        urlLink             <- map["link.url"]
        imageUrl            <- map["multimedia.src"]
        
    }
}
