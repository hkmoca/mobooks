//
//  SetCollectionViewProperties.swift
//  MoBooks
//
//  Created by Héctor Moreno on 18/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import UIKit

class SetCollectionViewProperties {

    func setCollectionViewProperties(collectionView: UICollectionView ){
        
        var collectionViewLayout: GridLayout!
        collectionViewLayout = GridLayout()
        collectionView.collectionViewLayout = collectionViewLayout
        collectionView.showsVerticalScrollIndicator = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.white

    }
}
