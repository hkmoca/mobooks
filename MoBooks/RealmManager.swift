//
//  RealmStructure.swift
//  MoBooks
//
//  Created by Héctor Moreno on 25/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import RealmSwift
class RealmManager {

   static let realm = try! Realm()
    
     static func writeToRealmDataBase(_ dataBasObject: Array<BaseDataFile>){
        try! self.realm.write {
            self.realm.add(dataBasObject, update: true)
            print("\(self.realm.configuration.fileURL!)")
        }
    }
    
    static func writeToDataBaseFacebookUser(_ facebookUser: Object){
        try! self.realm.write {
            self.realm.add(facebookUser, update: true)
        }
    }
}
