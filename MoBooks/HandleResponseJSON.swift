//
//  HandleResponse.swift
//  MoBooks
//
//  Created by Héctor Moreno on 08/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//


import Alamofire

class HandleResponseJSON {
    
   static func handleResponse(_ response: DataResponse<Any>, compleation: @escaping (_: [String : Any]) -> Void, onFailure: @escaping (_: NSError) -> Void){
        
        switch response.result {
        case .success(let JSON):
            if let json = JSON as? [String: AnyObject] {
                print("Got the JSON")
                compleation(json)
            } else {
                print("Incorrect JSON Format")
            }
        case .failure(let ERROR):
            onFailure(ERROR as NSError)
        }
    }
}
