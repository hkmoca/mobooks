//
//  FileInfo.swift
//  MoBooks
//
//  Created by Héctor Moreno on 27/01/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import RealmSwift

class BaseDataFile: Object {
    
    dynamic var title: String = ""
    dynamic var author: String = ""
    dynamic var contributor: String = ""
    dynamic var explanation: String = ""
    dynamic var rank: String = ""
    dynamic var urlLink: String = ""
    dynamic var imageUrl: String = ""
    
    override static func primaryKey() -> String? {
        return "title"
    }
}
