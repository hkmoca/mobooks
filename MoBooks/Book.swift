//
//  Book.swift
//  MoBooks
//
//  Created by Héctor Moreno on 27/01/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import ObjectMapper


class Book: BaseDataFile, Mappable {
    
    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        title               <- map["title"]
        author              <- map["author"]
        contributor         <- map["contributor"]
        explanation         <- map["description"]
        rank                <- map["ranks_history.rank"]
        
    }
}
