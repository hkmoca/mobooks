//
//  FacebookLogin.swift
//  MoBooks
//
//  Created by Héctor Moreno on 19/02/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//



import UIKit
import FBSDKLoginKit

class FBViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    let loginButton: FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions = ["email"]
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loginButton.delegate = self
        FaceboolManager.setLoginButton(view, loginButton: loginButton)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Did logout")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        
        FaceboolManager.getFacebookUserInformation()
        print("Successfully logged in")

    }
}
