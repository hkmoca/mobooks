//
//  customCell.swift
//  MoBooks
//
//  Created by Héctor Moreno on 24/01/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit
import Foundation
import AlamofireImage

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var elementImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        title.text = nil
        author.text = nil
    }
    
    func updateFile(cellInfo: BaseDataFile){
        
        title.text = cellInfo.title
        author.text = cellInfo.author
        
        if let imageURL = URL(string: cellInfo.imageUrl){
        elementImage.af_setImage(withURL: imageURL)
        }
    }
}
